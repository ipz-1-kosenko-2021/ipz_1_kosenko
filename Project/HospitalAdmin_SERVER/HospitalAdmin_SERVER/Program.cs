﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;


namespace HospitalAdmin_SERVER
{
    class PersonArr
    {
        public List<Person> PersonA { get; set; }

        public PersonArr()
        {
            PersonA = new List<Person>();
        }
    }
    class Person
    {
        public String UserID { get; set; }
        public String First_Name { get; set; }
        public String Last_Name { get; set; }
        public String Email { get; set; }
        public Person()
        {
            this.UserID = "";
            this.First_Name = "";
            this.Last_Name = "";
            this.Email = "";
           
        }
        public String toString()
        {
            return $"{First_Name},{Last_Name},{Email},{UserID}";
        }
    }
    class Patient
    {
        public String UserID { get; set; }
        public String First_Name { get; set; }
        public String Last_Name { get; set; }
        public String Email { get; set; }
        public Patient()
        {
            this.UserID = "";
            this.First_Name = "";
            this.Last_Name = "";
            this.Email = "";

        }
        public String toString()
        {
            return $"{First_Name},{Last_Name},{Email},{UserID}";
        }
    }
}
    class Program
    {
    static DateTime now = DateTime.Now;
    private static readonly Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    private static readonly List<Socket> heandler = new List<Socket>();
    private static readonly List<string> onlineusers = new List<string>(10) { null, null, null, null, null, null, null, null, null, null };
    private const int BUFFER_SIZE = 2048;
    private static readonly byte[] buffer = new byte[BUFFER_SIZE];
    private static void ReceiveCallback(IAsyncResult AR)
    {

        Socket current = (Socket)AR.AsyncState;
        int received;
        try
        {
            received = current.EndReceive(AR);
        }
        catch (SocketException ex)
        {
            Console.WriteLine($"Client {current.RemoteEndPoint.ToString()}  forcefully disconnected");
            // Don't shutdown because the socket may be disposed and its disconnected anyway.
            int i = heandler.IndexOf(current);
            current.Close();
            heandler.Remove(current);
            onlineusers.RemoveAt(i);
            return;
        }
        SqlConnection _sql;
        string connectionString = "Server=KOSENKOPC; Database= HospitalDB; Integrated Security = SSPI;";
        _sql = new SqlConnection(connectionString);

        _sql.Open();

        byte[] recBuf = new byte[received];

        try
        {
            Array.Copy(buffer, recBuf, received - 5);
        }
        catch (System.ArgumentOutOfRangeException exep)
        {
            // Don't shutdown because the socket may be disposed and its disconnected anyway.
            current.Shutdown(SocketShutdown.Both);
            current.Close();
            heandler.Remove(current);
            return;
        }

        string text = Encoding.ASCII.GetString(recBuf);
        Console.WriteLine("Received Text: " + text);
        

        if (text.Contains("closed"))
        {
            Console.WriteLine($"Client {current.RemoteEndPoint.ToString()} accurately disconnected");
            current.Shutdown(SocketShutdown.Both);
            current.Close();
            heandler.Remove(current);
            _sql.Close();
            return;
        }
        else if (text.Contains("log out"))
        {
            SqlDataReader dataReader;
            String Output = "";
            SqlCommand command = new SqlCommand(text.Replace("log out", "select First_Name, Last_Name from UserInfo where CONCAT(UserInfo.First_Name, ' ', UserInfo.Last_Name) ="), _sql);
            dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Output = dataReader.GetValue(0) + " " + dataReader.GetValue(1);
            }
            Console.WriteLine($"Client {current.RemoteEndPoint.ToString()} accurately logged out from account {Output}");
            onlineusers.Remove(Output);
            command.Dispose();
            dataReader.Close();
        }

        else if (text.Contains("check"))
        {

            SqlCommand sqlCommand = new SqlCommand(text.Replace("check", "select count(*) from UserInfo where"), _sql);
            int count = (int)sqlCommand.ExecuteScalar();
            if (count == 0)
            {
                sendData(current, "pass", now);
                Console.WriteLine("pass");
               
            }
            else
            {
                sendData(current, "fail", now);
                Console.WriteLine("fail");
            }
        }
        else if (text.Contains("zerop"))
        {

            SqlCommand sqlCommand = new SqlCommand(text.Replace("zerop", "select count(*) from patientInfo where"), _sql);
            int count = (int)sqlCommand.ExecuteScalar();
            if (count == 0)
            {
                sendData(current, "pass", now);
                Console.WriteLine("pass");

            }
            else
            {
                sendData(current, "fail", now);
                Console.WriteLine("fail");
            }
        }
        else if (text.Contains("pch"))
        {

            SqlCommand sqlCommand = new SqlCommand(text.Replace("pch", "select count(*) from patientInfo where"), _sql);
            int count = (int)sqlCommand.ExecuteScalar();
            if (count == 1)
            {
                sendData(current, "pass", now);
                Console.WriteLine("pass");

            }
            else
            {
                sendData(current, "fail", now);
                Console.WriteLine("fail");
            }
        }
        else if (text.Contains("select * from patientInfo where Last_name = "))
        {
            SqlDataReader dataReader;
            String Output = "";

            SqlCommand command = new SqlCommand(text, _sql);
            dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Output = dataReader.GetValue(0) + " " + dataReader.GetValue(1) + " " + dataReader.GetValue(2) 
                + " " + dataReader.GetValue(3) + " " + dataReader.GetValue(4) + " " + dataReader.GetValue(5) + " " + dataReader.GetValue(6) + " " + dataReader.GetValue(7);
            }
            dataReader.Close();
            command.Dispose();
            {

                sendData(current, Output, now);
                Console.WriteLine(Output);
            }

        }
        else if (text.Contains("onlineuser"))
        {
            SqlDataReader dataReader;
            String Output = "";
            SqlCommand command = new SqlCommand(text.Replace("add onlineuser", "select First_Name, Last_Name from UserInfo where CONCAT(UserInfo.First_Name, ' ', UserInfo.Last_Name) ="), _sql);
            dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Output = dataReader.GetValue(0) + " " + dataReader.GetValue(1);
            }
            int d = heandler.IndexOf(current);
            onlineusers.Insert(d, Output);
            dataReader.Close();
            command.Dispose();
        }
        else if (text.Contains("count(*)"))
        {
            SqlCommand sqlCommand = new SqlCommand(text, _sql);
            int count = (int)sqlCommand.ExecuteScalar();

            if (count == 1)
            {
                SqlDataReader dataReader;
                String Output = "";

                SqlCommand command = new SqlCommand(text.Replace("count(*)", "UserInfo.First_Name, UserInfo.Last_Name"), _sql);
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Output = dataReader.GetValue(0) + " " + dataReader.GetValue(1);
                }
                dataReader.Close();
                command.Dispose();
                if (!onlineusers.Contains(Output))
                {
                    sendData(current, "pass", now);
                    Console.WriteLine("pass");
                    

                    /*sendData(current, Output, now);
                    Console.WriteLine(Output);
                    int d = heandler.IndexOf(current);
                    onlineusers.Insert(d, Output);*/
                }
                else
                {
                    sendData(current, $"{Output} is already logged in", now);
                    Console.WriteLine($"{Output} is already logged in");
                }
            }
            else
            {
                sendData(current, "fail", now);
                Console.WriteLine("fail1");
            }
            sqlCommand.Dispose();
        }
        else if (text.Contains("update"))
        {
            SqlCommand command = new SqlCommand(text, _sql);
            SqlDataAdapter adapter = new SqlDataAdapter();

            adapter.UpdateCommand = new SqlCommand(text, _sql);
            try
            {
                adapter.UpdateCommand.ExecuteNonQuery();
                sendData(current, "pass", now);
                Console.WriteLine("pass");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Wrong command - {0}", text);
                sendData(current, "fail2", now);
            }

            command.Dispose();
            adapter.Dispose();
        }
        /*else if (
                   text.Contains("select * from UserInfo, Grade where UserInfo.UserId = Grade.UserID") ||
                   text.Contains("select top(5) * from UserInfo, Grade where UserInfo.UserId = Grade.UserID") ||
                   text.Contains("CONCAT"))
        {
            SqlDataReader dataReader;
            String Output = "";

            SqlCommand command = new SqlCommand(text, _sql);
            dataReader = command.ExecuteReader();

            PersonArr parr = new PersonArr();
            Person student = new Person();

            int x = 0;
            while (dataReader.Read())
            {
                try
                {
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        if (i == 0)
                        {
                            parr.PersonA.Add(new Person());
                            x = parr.PersonA.Count - 1;
                        }
                        try
                        {
                            if (dataReader.GetName(i) == "First_Name")
                            {
                                parr.PersonA[x].First_Name = $"{dataReader.GetValue(i)}";
                            }
                            else if (dataReader.GetName(i) == "Last_Name")
                            {
                                parr.PersonA[x].Last_Name = $"{dataReader.GetValue(i)}";
                            }
                            else if (dataReader.GetName(i) == "UserID")
                            {
                                parr.PersonA[x].UserID = $"{dataReader.GetValue(i)}";
                            }
                            else if (dataReader.GetName(i) == "Email")
                            {
                                parr.PersonA[x].Email = $"{dataReader.GetValue(i)}";
                            }
                            else if (dataReader.GetName(i) == "GroupKI")
                            {
                                parr.PersonA[x].Group = $"{dataReader.GetValue(i)}";
                            }
                            else if (dataReader.GetName(i) == "SP")
                            {
                                parr.PersonA[x].SP_Grade = $"{dataReader.GetValue(i)}";
                            }
                            else if (dataReader.GetName(i) == "RC")
                            {
                                parr.PersonA[x].RC_Grade = $"{dataReader.GetValue(i)}";
                            }
                            else if (dataReader.GetName(i) == "OBD")
                            {
                                parr.PersonA[x].ODB_Grade = $"{dataReader.GetValue(i)}";
                            }
                            else if (dataReader.GetName(i) == "AC")
                            {
                                parr.PersonA[x].AC_Grade = $"{dataReader.GetValue(i)}";
                            }
                            else if (dataReader.GetName(i) == "IPZ")
                            {
                                parr.PersonA[x].IPZ_Grade = $"{dataReader.GetValue(i)}";
                            }
                            else if (dataReader.GetName(i) == "AVG_grade")
                            {
                                parr.PersonA[x].AVG_Grade = $"{dataReader.GetValue(i)}";
                            }
                        }
                        catch (Exception ex)
                        {
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    sendData(current, "List with all students: failed", now);
                    Console.WriteLine("List with all students: failed");
                    break;
                }
            }
            if (!text.Contains("CONCAT"))
            {
                Output = JsonConvert.SerializeObject(parr, Formatting.Indented);
                sendData(current, Output, now);
                Console.WriteLine($"List with all students returned \n{Output}");
            }
            else
            {
                sendData(current, parr.PersonA[0].toString(), now);
                Console.WriteLine($"Student info returned \n{parr.PersonA[0].toString()}");
            }
            dataReader.Close();
            command.Dispose();
        }*/
        else if (text.Contains("add_patient_info"))
        {
            SqlCommand command;
            SqlDataAdapter adapter = new SqlDataAdapter();

            command = new SqlCommand(text, _sql);

            adapter.InsertCommand = new SqlCommand(text, _sql);
            try
            {
                adapter.InsertCommand.ExecuteNonQuery();
                sendData(current, "pass", now);
                Console.WriteLine("pass");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Wrong command - {0}", text);
                sendData(current, "fail", now);
                Console.WriteLine("fail3");
            }

            command.Dispose();
        }

        else if (text.Contains("addinfo"))
        {
            SqlCommand command;
            SqlDataAdapter adapter = new SqlDataAdapter();

            command = new SqlCommand(text, _sql);

            adapter.InsertCommand = new SqlCommand(text, _sql);
            try
            {
                adapter.InsertCommand.ExecuteNonQuery();
                sendData(current, "pass", now);
                Console.WriteLine("pass");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Wrong command - {0}", text);
                sendData(current, "fail", now);
                Console.WriteLine("fail3");
            }

            command.Dispose();
        }
        

        _sql.Close();
        current.BeginReceive(buffer, 0, BUFFER_SIZE, SocketFlags.None, ReceiveCallback, current);
    }
    public static void sendData(Socket handler, string data, DateTime now)
    {
        byte[] msg = Encoding.ASCII.GetBytes(data);
        handler.Send(msg);
        
    }
    private static void CloseAllSockets()
    {
        foreach (Socket socket in heandler)
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }

        listener.Close();
    }

    private static void AcceptCallback(IAsyncResult AR)
    {
        Socket socket;

        try
        {
            socket = listener.EndAccept(AR);
        }
        catch (ObjectDisposedException ex) // I cannot seem to avoid this (on exit when properly closing sockets)
        {
            return;
        }

        heandler.Add(socket);
        socket.BeginReceive(buffer, 0, BUFFER_SIZE, SocketFlags.None, ReceiveCallback, socket);
        Console.WriteLine($"Client {socket.RemoteEndPoint.ToString()}  connected, waiting for request...");
        listener.BeginAccept(AcceptCallback, null);
    }
    private static void SetupServer()
    {
        Console.WriteLine("Setting up server...");
        listener.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 27000));
        listener.Listen(5);
        listener.BeginAccept(AcceptCallback, null);
        Console.WriteLine("Server setup complete");
    }
    static void Main(string[] args)
    {
        Console.Title = "Server";
        if (!Directory.Exists("log"))
        {
            Directory.CreateDirectory("log");
        }

        SetupServer();
        Console.ReadLine(); // When we press enter close everything
        CloseAllSockets();
    }

}


