﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hospital
{
    /// <summary>
    /// Логика взаимодействия для GetAllInfoWindow.xaml
    /// </summary>
    public partial class GetAllInfoWindow : Window
    {
        private Socket senderSocket;
        private String TextBoxSearch;
        public GetAllInfoWindow(Socket senderSocket)
        {
            this.senderSocket = senderSocket;
            
            InitializeComponent();
        }

       

        private void Button_Menu_Click(object sender, RoutedEventArgs e)
        {
            InfoPatientWindow InfoPatientWindow = new InfoPatientWindow(this.senderSocket, this.TextBoxSearch);
            InfoPatientWindow.Show();
            Hide();
        }


        private void Button_Edit_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
