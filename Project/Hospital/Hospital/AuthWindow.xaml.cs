﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hospital
{
    /// <summary>
    /// Логика взаимодействия для AuthWindow.xaml
    /// </summary>
    public partial class AuthWindow : Window
    {
        private Socket senderSocket;

        public AuthWindow(Socket senderSocket)
        {
            this.senderSocket = senderSocket;
            InitializeComponent();
        }

        private void Button_Auth_Click(object sender, RoutedEventArgs e)
        {
            string login = TextBoxLogin.Text.Trim();    //Trim() - Забирає пробіли на початку та в кінці строки
            string pass = passBox.Password.Trim();

            if (login.Length < 5 || !login.Contains("@") || !login.Contains("."))        //Якщо довжина менша трьох символів
            {
                TextBoxLogin.ToolTip = "Перевірте правильність введених символів!";  //ToolTip - добавляє підказку, коли користувач наводить на поле
                TextBoxLogin.Background = Brushes.IndianRed;
            }
            else if (pass.Length < 5)
            {
                passBox.ToolTip = "Довжина паролю повинна бути більше 5 символів!";
                passBox.Background = Brushes.IndianRed;
            }
            else
            {
                TextBoxLogin.ToolTip = "";
                TextBoxLogin.Background = Brushes.Transparent;      // Transparent - Забирає наш червоний колір(помилки), при коректних введених даних
                passBox.ToolTip = "";
                passBox.Background = Brushes.Transparent;

                
                    bool passVerification = false;

                    Hospital.Program.sendData(senderSocket, $"select count(*) from UserInfo where Email = '{login}' and Pass = '{pass}'");
                    String v1 = Hospital.Program.receiveData(senderSocket);

                    if (v1.Contains("pass"))
                    {
                        passVerification = true;
                    }

                    if (passVerification)
                    {
                        
                        SearchPatientWindow lmf = new SearchPatientWindow (senderSocket);
                        lmf.Show();
                        this.Hide();
                }
                    else if (v1.Contains("is already logged in"))
                    {
                        MessageBox.Show("Цей користувач уже ввійшов у систему!");
                    }
                    else if (v1.Contains("fail"))
                    {
                        MessageBox.Show("Неправильні облікові дані!");
                    }

                }
               
            

        }

        private void Button_Window_Reg_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow(this.senderSocket);
            mainWindow.Show();
            Hide();
        }
    }
}
