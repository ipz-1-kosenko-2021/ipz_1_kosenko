﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hospital
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Socket senderSocket;

        public MainWindow(Socket senderSocket)
        {
            this.senderSocket = senderSocket;
            InitializeComponent();
        }

        private void Button_Reg_Click(object sender, RoutedEventArgs e)
        { 
            string login = TextBoxLogin.Text.Trim();    //Trim() - Забирає пробіли на початку та в кінці строки
            string pass = passBox.Password.Trim();
            string pass_2 = passBox_2.Password.Trim();


            if (pass.Length < 5)
            {
                passBox.ToolTip = "Довжина паролю повинна бути більше 5 символів!";
                passBox.Background = Brushes.IndianRed;
            }
            else if (pass != pass_2)
            {
                passBox_2.ToolTip = "Перевірте правильність введених символів!";
                passBox_2.Background = Brushes.IndianRed;
            }
            else if (login.Length < 5 || !login.Contains("@") || !login.Contains("."))
            {
                TextBoxLogin.ToolTip = "Перевірте правильність введених символів!";
                TextBoxLogin.Background = Brushes.IndianRed;
            }
            else
            {
                TextBoxLogin.ToolTip = "";
                TextBoxLogin.Background = Brushes.Transparent;      // Transparent - Забирає наш червоний колір(помилки), при коректних введених даних
                passBox.ToolTip = "";
                passBox.Background = Brushes.Transparent;
                passBox_2.ToolTip = "";
                passBox_2.Background = Brushes.Transparent;
                TextBoxLogin.ToolTip = "";
                TextBoxLogin.Background = Brushes.Transparent;

                MessageBox.Show("Все добре!");
                bool dataExists = true;
                //Hospital.Program.sendData(senderSocket, $"select count(*) from UserInfo where Email like '{this.logintextBox.Text}'", now);
                Hospital.Program.sendData(senderSocket, $"check Email like '{login}'");
                String v1 = Hospital.Program.receiveData(senderSocket);

                if (v1 == "pass")
                {
                    dataExists = false;
                }
                else
                {
                    dataExists = true;
                }

                if (!dataExists)
                {
                    Hospital.Program.sendData(senderSocket,
                        $"exec addinfo  '{login}', '{NametextBox.Text}', '{SurnametextBox.Text}', '{pass}'");

                    v1 = Hospital.Program.receiveData(senderSocket);

                    if (v1 == "pass")
                    {
                        MessageBox.Show("Реєстрація успішна!");
                       // Hospital.Program.sendData(senderSocket, $"add onlineuser '{NametextBox.Text} {SurnametextBox.Text}'");
                        //this.Hide();
                        //SearchPatientWindow lmf = new SearchPatientWindow(senderSocket, Hospital.Program.receiveData(senderSocket));
                       // lmf.Show();
                    }
                }
                else
                {
                    MessageBox.Show("E-mail вже існує!");
                }
            }
        }
        

        private void Button_Window_Auht_Click(object sender, RoutedEventArgs e)
        {
            AuthWindow authWindow = new AuthWindow(this.senderSocket);
            authWindow.Show();
            Hide();
        }
    }
}
