﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hospital
{
    /// <summary>
    /// Логика взаимодействия для SearchPatientWindow.xaml
    /// </summary>
    public partial class SearchPatientWindow : Window
    {
        private Socket senderSocket;
        

        public SearchPatientWindow(Socket senderSocket)
        {
            this.senderSocket = senderSocket;
            InitializeComponent();
        }


        private void Button_Search_Click(object sender, RoutedEventArgs e)
        {
            
            if (TextBoxSearch != null)
            {
                Hospital.Program.sendData(senderSocket,
                                   $"pch Last_name = '{TextBoxSearch.Text}'");
                String v1 = Hospital.Program.receiveData(senderSocket);
                if (v1 == "pass")
                {
                    InfoPatientWindow InfoPatientWindow = new InfoPatientWindow(this.senderSocket, TextBoxSearch.Text);
                    InfoPatientWindow.Show();
                    Hide();
                }
            }
            else
            {
                MessageBox.Show("Поле пусте");
            }
        }

        private void Button_Add_Click(object sender, RoutedEventArgs e)
        {
           AddPatientWindow AddPatientWindow = new AddPatientWindow(this.senderSocket);
           AddPatientWindow.Show();
            Hide();
        }

    }
}
