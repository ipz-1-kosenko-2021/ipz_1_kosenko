﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace Hospital
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
       
        public static Socket StartClient2(Socket sender)
        {
            while (sender == null)
            {
                sender = StartClient("127.0.0.1", "27000");
                if (sender != null)
                {
                    return sender;
                }
                else
                {
                    MessageBox.Show("Try to start client again ?", "Server not found");
                }
            }
            return sender;
        }
        public static bool IsValid(string emailaddress)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            Match match = regex.Match(emailaddress);

            if (match.Success)
                return true;
            else
                return false;
        }
        public static void sendData(Socket sender, String str)
        {
            try
            {
                byte[] msg = Encoding.ASCII.GetBytes($"{str}<EOF>");
                sender.Send(msg);
            }
            catch (Exception e)
            {

            }
        }
        public static String receiveData(Socket sender)
        {
            try
            {
                byte[] bytes = new byte[16384];
                int bytesRec = sender.Receive(bytes);
                String receivedMessage = $"{Encoding.ASCII.GetString(bytes, 0, bytesRec)}";
                return receivedMessage;
            }
            catch (Exception e)
            {
                MessageBox.Show("Server disconnected");
                return "close";
            }
        }
        public static Socket StartClient(String ip, String Port)
        {
            byte[] bytes = new byte[1024];

            Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                sender.Connect(IPAddress.Parse(ip), Convert.ToInt32(Port));

            }
            catch (Exception e)
            {
                return null;
            }

            return sender;
        }
    }

}

