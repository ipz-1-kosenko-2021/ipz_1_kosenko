﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hospital
{
  public partial class InfoPatientWindow : Window
    {
        private Socket senderSocket;
        private String TextBoxSearch;
        private Point lastPoint;

        public InfoPatientWindow(Socket senderSocket, String TextBoxSearch)
        {
            this.senderSocket = senderSocket;
            this.TextBoxSearch = TextBoxSearch;
            Hospital.Program.sendData(senderSocket,
                $"select * from patientInfo where Last_name = '{TextBoxSearch}' ");
            String v1 = Hospital.Program.receiveData(senderSocket);


            IList<String> values = v1.Split(',').ToList<String>();

           PatientNameTextBox = new TextBox();
            PatientSurNameTextBox = new TextBox();
            PatientLocation = new TextBox();
            PatientYear = new TextBox();
            PatientDoctor = new TextBox();
            PatientPassID = new TextBox();
            

            InitializeComponent();

            PatientNameTextBox.Text = values[0];
            PatientSurNameTextBox.Text = values[1];
            PatientLocation.Text = values[2];
            PatientYear.Text = values[3];
            PatientDoctor.Text = values[4];
            PatientPassID.Text = values[5];
           
            PatientNameTextBox.IsEnabled = false;
            PatientSurNameTextBox.IsEnabled = false;
            PatientLocation.IsEnabled = false;
            PatientYear.IsEnabled = false;
            PatientDoctor.IsEnabled = false;
            PatientPassID.IsEnabled = false;

        }

       /* private void Button_Appoint_Click(object sender, RoutedEventArgs e)
        {
            AppointSurveyWindow AppointSurveyWindow = new AppointSurveyWindow(this.senderSocket);
            AppointSurveyWindow.Show();
            Hide();
        }*/


        private void Button_All_Info_Click(object sender, RoutedEventArgs e)
        {
            GetAllInfoWindow GetAllInfoWindow = new GetAllInfoWindow(this.senderSocket);
            GetAllInfoWindow.Show();
            Hide();
        }
    }
}
