﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hospital
{
    /// <summary>
    /// Логика взаимодействия для AddPatientWindow.xaml
    /// </summary>
    public partial class AddPatientWindow : Window
    {
        private Socket senderSocket;

        public AddPatientWindow(Socket senderSocket)
        {
            this.senderSocket = senderSocket;
            InitializeComponent();
        }


        private void Button_Window_Search_Click(object sender, RoutedEventArgs e)
        {
            SearchPatientWindow SearchPatientWindow = new SearchPatientWindow(this.senderSocket);
            SearchPatientWindow.Show();
            Hide();
        }

        private void Button_Reg_Click(object sender, RoutedEventArgs e)
        {
            string Name = PatientNameTextBox.Text.Trim();    //Trim() - Забирає пробіли на початку та в кінці строки
            string SurName = PatientSurNameTextBox.Text.Trim();
            string email = TextBoxEmail.Text.Trim().ToLower();

            if (Name.Length < 2)        //Якщо довжина менша трьох символів
            {
                PatientNameTextBox.ToolTip = "Розмір імені повинен бути більше однієї букви!";  //ToolTip - добавляє підказку, коли користувач наводить на поле
                PatientNameTextBox.Background = Brushes.IndianRed;
            }
            else if (SurName.Length < 2)
            {
                PatientSurNameTextBox.ToolTip = "Розмір прізвища повинен бути більше однієї букви!";
                PatientSurNameTextBox.Background = Brushes.IndianRed;
            }
            else if (email.Length < 3 || !email.Contains("@") || !email.Contains("."))
            {
                TextBoxEmail.ToolTip = "Перевірте коректність введених даних!";
                TextBoxEmail.Background = Brushes.IndianRed;
            }
            else
            {
                PatientNameTextBox.ToolTip = "";
                PatientNameTextBox.Background = Brushes.Transparent;      // Transparent - Забирає наш червоний колір(помилки), при коректних введених даних
                PatientSurNameTextBox.ToolTip = "";
                PatientSurNameTextBox.Background = Brushes.Transparent;
                TextBoxEmail.ToolTip = "";
                TextBoxEmail.Background = Brushes.Transparent;

                MessageBox.Show("Все добре!");
                bool dataExists = true;
                Hospital.Program.sendData(senderSocket, $"zerop Email like '{email}'");
                String v1 = Hospital.Program.receiveData(senderSocket);

                if (v1 == "pass")
                {
                    dataExists = false;
                }
                else
                {
                    dataExists = true;
                }

                if (!dataExists)
                {
                    Hospital.Program.sendData(senderSocket,
                        $"exec add_patient_info  '{email}', '{PatientNameTextBox.Text}', '{PatientSurNameTextBox.Text}'");


                    
                    SearchPatientWindow lmf = new SearchPatientWindow(senderSocket);
                    lmf.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Email вже існує!");
                }

            }
        }
    }
}
